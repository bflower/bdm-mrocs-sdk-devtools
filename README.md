# bdm-mrocs-sdk-devtools

MROCS SDK Devtool companion

## Usage
````javascript
import { getDev, setFixtures } from '@b-flower/bdm-mrocs-sdk-devtools'

setFixtures({...})

getDev(action).then(res)

````

## API

* `get`
* `dimensions`
* `setFixtures`
* `getFixtureAll`
* `getFixture`


#### Usage

````javascript
// __DEV__ is explained bellow
if (__DEV__) {
  const { get, dimensions, setFixtures } = require('@b-flower/bdm-mrocs-sdk-devtools')
  // see fixtures below
  const fixtures = require('./fixtures')
  setFixtures(fixtures)
  module.exports = { get, dimensions }
} else {
  module.exports = require('@b-flower/bdm-mrocs-sdk')
}

````

#### Ensure to switch to production mode

To make sure we’re using right mode in the development or production builds, we will envify our code.
You can use [`DefinePlugin`](https://github.com/webpack/docs/wiki/list-of-plugins#defineplugin) with Webpack, or [`envify`](https://github.com/zertosh/loose-envify) for Browserify.

With Webpack, you'll need two config files, one for development and one for production. Here's a snippet from an example production config:

##### `webpack.config.prod.js`

```js
// ...
plugins: [
  new webpack.DefinePlugin({
    '__DEV__', false,
  })
],
// ...
```

##### `webpack.config.dev.js`

```js
// ...
plugins: [
  new webpack.DefinePlugin({
    '__DEV__', true,
  })
],
// ...
```


## `get`

```javascript
function get(action: Enum, options: Object): Promise<DataObject>
```

#### `action` argument

_(Enum)_: required

Refers to `:endpoint` in mentioned web API documentation

An enum list is available in `ACTION_LIST` object

##### `action list`

see [`@b-flower/bdm-mrocs-sdk`](https://www.npmjs.com/package/@b-flower/bdm-mrocs-sdk)

#### `options` argument
see [`@b-flower/bdm-mrocs-sdk`](https://www.npmjs.com/package/@b-flower/bdm-mrocs-sdk)

Effects with `options` on `devtools` depend on your implementation of `fixture` part.


#### Return

`get` return a `promise` resolved with `DataObject`.

Please refer to the web API documentation **b-eden - API v1 Report** and **b-eden - API v1 Module** to determine DataObject properties for each method.

## `dimensions`

```javascript
function dimensions({ width: Number, height: Number }): void
```

Send inner content dimensions to b-eden.
In DEV environment, DevTools only logs `{ width, height }`

#### Usage (example)
```
  componentDidMount() {
    this.props.sdk.dimensions({
      width: myRef.clientWidth,
      height: myRef.clientHeight,
    })
  }
```

## `getFixtureAll` & `getFixture`

see [`fixtures.md`](./fixtures.md)