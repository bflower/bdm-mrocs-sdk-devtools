import Promise from 'bluebird'
import { ensureActionOk } from './actions'
import { ERR_FIXTURE_NOT_AVAILABLE } from './error_codes'

let DEV_FIXTURES = {}


export function setFixtures(fixtures) {
  if (fixtures) DEV_FIXTURES = fixtures
}


export function get(action, options = {}) {
  return Promise.try(() => {
    ensureActionOk(action)
    if (typeof DEV_FIXTURES === 'function') {
      return DEV_FIXTURES(action, options)
    }

    if (action in DEV_FIXTURES) {
      if (typeof DEV_FIXTURES[action] === 'function') {
        return DEV_FIXTURES[action](options)
      }
      return DEV_FIXTURES[action]
    }

    const e = new Error(`${action} not available in fixtures`)
    e.code = ERR_FIXTURE_NOT_AVAILABLE
    throw e
  })
}

export function dimensions({ width, height }) {
  return console.log('dimensions', { width, height })
}