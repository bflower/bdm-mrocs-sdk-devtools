export const ERR_UNKNOWN_ACTION = 'bmsUnknownAction'
export const ERR_FIXTURE_NOT_AVAILABLE = 'bmsFixtureNotAvailable'
