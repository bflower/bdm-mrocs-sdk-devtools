import { ACTION_LIST } from '../actions'

// PARTICIPATION: 'report/participation',
// RESULTS: 'report/results',
// LEARNERS_COUNT: 'report/learners/count',
// LEARNERS: 'report/learners',
// INTERACTIONS_COUNT: 'report/interactions/count',
// INTERACTIONS: 'report/interactions',
// INTERACTIONS_LIST: 'report/interactions/list',
// MOD_INFO: 'module/info',
// MOD_INTERACTIONS: 'module/interactions',

const FIXTURE_LIST =  {
  [ACTION_LIST.PARTICIPATION]: require('./report_participation.json'),
  [ACTION_LIST.RESULTS]: require('./report_results.json'),
  [ACTION_LIST.LEARNERS_COUNT]: require('./report_learners_count.json'),
  [ACTION_LIST.LEARNERS]: require('./report_learners.json'),
  [ACTION_LIST.INTERACTIONS_COUNT]: require('./report_interactions_count.json'),
  [ACTION_LIST.INTERACTIONS]: require('./report_interactions.json'),
  [ACTION_LIST.INTERACTIONS_LIST]: require('./report_interactions_list.json'),
  [ACTION_LIST.MOD_INFO]: require('./module_info.json'),
  [ACTION_LIST.MOD_INTERACTIONS]: require('./module_interactions.json'),
}

export function getFixtureAll() {
  return FIXTURE_LIST
}

export function getFixture(action) {
  if (action in FIXTURE_LIST) return FIXTURE_LIST[action]
}