## `fixture`

`fixture` accepts 3 forms

### Object `fixture`

````javascript
// ./fixture.js
import { ACTION_LIST } from '@b-flower/bdm-mrocs-sdk'
export default {
  // action              DataObject
  [ACTION_LIST.RESULTS]: require('result.json'),
  [ACTION_LIST.LEARNERS_COUNT]: 10,
}

````

In `devEnv`, if you don't provide a action that you use in `getDev`, `getDev` will throw a `ERR_FIXTURE_NOT_AVAILABLE` error.

#### Usage
````javascript
// ./mrocs_sdk.js
if (__DEV__) {
  const { getDev, setFixtures } = require('@b-flower/bdm-mrocs-sdk-devtools')
  const fixtures = require('./fixtures')
  setFixtures(fixtures)
  module.exports = getDev
} else {
  const { get } = require('@b-flower/bdm-mrocs-sdk')
  module.exports = get
}

````

````javascript
// ./run_fixture.js
import { ACTION_LIST } from '@b-flower/bdm-mrocs-sdk'
import getSDK from './mrocs_sdk'

getSDK(ACTION_LIST.RESULTS)
  .then(...)
// => is ok

getSDK(ACTION_LIST.LEARNER)
  .then(...)
// => throw an eror - LEARNER is not defined in fixture


getSDK(ACTION_LIST.LEARNERS_COUNT)
  .then(...)
// => is ok

````

### Object with function `fixture`

````javascript
// fixture.js
import { ACTION_LIST } from '@b-flower/bdm-mrocs-sdk'
export default {
  // action
  [ACTION_LIST.RESULTS]: function(options) {
    const DataObject = {....}
    return DataObject
  },
  [ACTION_LIST.LEARNERS_COUNT]: function(options) {
    return 10
  },
}

````

In `devEnv`, if you don't provide a action that you use in `getDev`, `getDev` will throw a `ERR_FIXTURE_NOT_AVAILABLE` error.

Usage is the same as Object `fixture`

### function `fixture`

````javascript
// fixture.js
export default function(action, options) {
  ...

  return DataObject // according to action
}

````


## `fixture` helpers

To help you to develop easily, we provide helpers fixture.

### `getFixtureAll`

````javascript
function getFixtureAll(): Object
````

#### Return

````javascript
// implemenation of getFixtureAll()
{
  'report/participation': require('./report_participation.json'),
  'report/results': require('./report_results.json'),
  'report/learners/count': require('./report_learners_count.json'),
  'report/learners': require('./report_learners.json'),
  'report/interactions/count': require('./report_interactions_count.json'),
  'report/interactions': require('./report_interactions.json'),
  'report/interactions/list': require('./report_interactions_list.json'),
  'module/info': require('./module_info.json'),
  'module/interactions': require('./module_interactions.json'),
}
````


#### Usage

````javascript
// ./mrocs_sdk.js
if (__DEV__) {
  const { getDev, setFixtures, getFixtureAll } = require('@b-flower/bdm-mrocs-sdk-devtools')
  // see fixtures below
  setFixtures(getFixtureAll())
  module.exports = getDev
} else {
  const { get } = require('@b-flower/bdm-mrocs-sdk')
  module.exports = get
}
````



### `getFixture`

````javascript
function getFixture(action: Enum): DataObject
````

#### Usage

````javascript
// ./mrocs_sdk.js
if (__DEV__) {
  const { ACTION_LIST } = require('@b-flower/bdm-mrocs-sdk')
  const { getDev, setFixtures } = require('@b-flower/bdm-mrocs-sdk-devtools')

  setFixtures({
    [ACTION_LIST.PARTICIPATION]: {
      "data": {
          "total": 485,
          "started": 437,
          "completed": 435,
          "finalized": 435,
          "successful": 2
      }
    },
    ...
  })
  module.exports = getDev
} else {
  const { get } = require('@b-flower/bdm-mrocs-sdk')
  module.exports = get
}
````

